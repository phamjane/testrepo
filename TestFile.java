import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.dom.client.Style;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.Image;

public class Test
{
	public void main()
	{
		AbstractCell<String> cell = new AbstractCell<String>()
		{
			@Override
			public void render(Context context, String value, SafeHtmlBuilder sb)
			{
				sb.appendEscaped(value);
				Image image = new Image();
				image.setWidth("14px");
				image.setHeight("14px");
				image.setTitle("This object is invalid.");
				addImages(sb, image);
				// Test change
				// Another change
			}
		};
	}
	
	private void addImages(SafeHtmlBuilder sb, Image... images)
	{
		for (Image image : images)
		{
			image.getElement().getStyle().setFloat(Style.Float.RIGHT);
			String imageHtml = image.getElement().getInnerHTML();
			
			sb.appendEscaped("<span style=\"white-space:nowrap;overflow:hidden;text-overflow:ellipsis;width:100%;");
			sb.appendEscaped(imageHtml);
			sb.appendEscaped("</span>");
			sb.appendEscaped("<div style=\"clear:right;\"></div>");
		}
	}

}
